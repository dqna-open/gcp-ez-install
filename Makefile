cloud_login:
	# Please use an account that has rights to create a new project
	gcloud auth login

# 1 GCP, Attach Billing account, Enable APIs, Create Cloud Storage
the_gcp_scaffold:
	@python3 ./executors/create_project_scaffold.py

# 2. Create Auth0 scaffold
the_auth0_configuration:
	@python3 ./executors/auth0.py

# 3. Download the repos and build and push the images to the
the_images:
	@python3 ./executors/build_sources.py

the_secrets:
	@python3 ./executors/secret_creator.py
	@python3 ./executors/apply_secrets.py

the_app:
	@python3 ./executors/manifester.py
	@python3 ./executors/create_apps.py

the_database_migrations:
	@python3 ./executors/migrate.py

the_mailboxcredentials:
	@python3 ./executors/create_credentials.py
	@python3 ./executors/secret_creator_sa360ms.py
	@python3 ./executors/apply_secrets.py