import subprocess
import os
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent


def execute_command(command: str):
    process = subprocess.Popen(
        command,
        stdout=subprocess.PIPE,
        universal_newlines=True,
        shell=True
    )

    while True:
        output = process.stdout.readline()
        print(output.strip())
        # Do something else
        return_code = process.poll()
        if return_code is not None:
            # Process has finished, read rest of the output
            for output in process.stdout.readlines():
                print(output.strip())
            break


def user_prompt_for_confirm(question: str) -> bool:
    """ Prompt the yes/no-*question* to the user. """
    from distutils.util import strtobool

    while True:
        user_input = input(question + " [y/n]: ").lower()
        try:
            return bool(strtobool(user_input))
        except ValueError:
            print("Please use y/n or yes/no.\n")


################################################################################
# Migrate the database
################################################################################
with open(os.path.join(BASE_DIR, 'project_id'), 'r') as fh:
    project_id = fh.read()
    if len(project_id) == 0:
        PROJECT_ID = input("Please provide the Project ID: ")
    else:
        PROJECT_ID = project_id
        print(f"Migrating !!! {PROJECT_ID}")
        
        
execute_command('while [[ $(kubectl get pod --selector=name=db -o \'jsonpath={..status.conditions[?(@.type=="Ready")].status}\') != "True" ]]; do echo "waiting for pod" && sleep 1; done')
execute_command(
    'kubectl exec -ti --namespace precision-dashboard $(kubectl get pod --namespace precision-dashboard  -l name=api -o jsonpath="{.items[0].metadata.name}") -- ./manage.py migrate'
)
