import subprocess
import os

from pathlib import Path
BASE_DIR = Path(__file__).resolve().parent.parent

def execute_command(command: list):
    process = subprocess.Popen(command,
                               stdout=subprocess.PIPE,
                               universal_newlines=True)

    while True:
        output = process.stdout.readline()
        print(output.strip())
        # Do something else
        return_code = process.poll()
        if return_code is not None:
            # Process has finished, read rest of the output
            for output in process.stdout.readlines():
                print(output.strip())
            return return_code


def user_prompt_for_confirm(question: str) -> bool:
    """ Prompt the yes/no-*question* to the user. """
    from distutils.util import strtobool

    while True:
        user_input = input(question + " [y/n]: ").lower()
        try:
            return bool(strtobool(user_input))
        except ValueError:
            print("Please use y/n or yes/no.\n")


print("################################################################################")
print("# This will create the Google Cloud Project and does some scaffolding          #")
print("# --- PLEASE MAKE SURE YOU ARE AUTHENTICATED BEFORE PROCEEDING!!!----          #")
print("################################################################################")


if not user_prompt_for_confirm("Do you want to use an existing project?"):


    ################################################################################
    # Create project
    ################################################################################

    PROJECT_ID = input('Give the project name you want to create: ')
    if not user_prompt_for_confirm(f'The Project ID you entered is "{PROJECT_ID}". Would you like to continue?'):
        print("OK, bye bye")
        exit(0)

    with open(os.path.join(BASE_DIR, 'project_id'), 'w') as fh:
        fh.write(PROJECT_ID)

    res = execute_command(['gcloud', 'projects', 'create', PROJECT_ID])
    if res != 0:
        print("There was a problem... See message above")
        if not user_prompt_for_confirm(f'You still want to continue?'):
            print("OK, bye bye")
            exit(res)

    print('################################################################################')
    print('# Attach billing account')
    print('################################################################################')
    execute_command(['gcloud', 'config', 'set', 'project', PROJECT_ID])

    BILLING_ACCOUNT = input('Give the Billing account ID: ')
    if not user_prompt_for_confirm(f'The Billing account ID you entered is "{BILLING_ACCOUNT}". Is this correct?'):
        print("OK, bye bye")
        exit(0)
    command = ['gcloud', 'alpha', 'billing', 'projects', 'link', PROJECT_ID, '--billing-account', BILLING_ACCOUNT]
    res = execute_command(command)

    if res != 0:
        print("There was a problem... Exiting")
        exit(res)
else:
    PROJECT_ID = input('Give the project name you want to install the GPD on: ')
    if not user_prompt_for_confirm(f'The Project ID you entered is "{PROJECT_ID}". Would you like to continue?'):
        print("OK, bye bye")
        exit(0)

    with open(os.path.join(BASE_DIR, 'project_id'), 'w') as fh:
        fh.write(PROJECT_ID)

    execute_command(['gcloud', 'config', 'set', 'project', PROJECT_ID])

print('################################################################################')
print('# Enable APIs')
print('################################################################################')

apis = [
    'container.googleapis.com',
    'storage.googleapis.com',
    'analytics.googleapis.com',
    'gmail.googleapis.com',
    'cloudbuild.googleapis.com',
    'sheets.googleapis.com'
]
for api in apis:
    command = ['gcloud', 'services', 'enable', api]
    print(f"Trying to enable the API {api}")
    res = execute_command(command)

    if res != 0:
        print("One of the APIs could not be enabled... Exiting")
        exit(res)
    print(f"API {api} enabled!")

print("Please go to the project and check if the Kubernetes API is enabled")
print("This can take a couple of minutes. Please be patient")

print('################################################################################')
print('# Create Cluster and Namespace')
print('################################################################################')
CLUSTER_NAME = 'gpd-cluster'
CLUSTER_ZONE = 'europe-west4-a'

print(f'We are going to create a cluster in "{CLUSTER_ZONE}" with name "{CLUSTER_NAME}"')
if user_prompt_for_confirm('Do you want to modify the zone?'):
    CLUSTER_ZONE = input('Enter the cluster zone (https://cloud.google.com/compute/docs/regions-zones#available)')
    if not user_prompt_for_confirm(f'The zone you entered is "{CLUSTER_ZONE}". Would you like to continue?'):
        print("OK, bye bye")
        exit(0)

    with open(os.path.join(BASE_DIR, 'cluster_zone'), 'w') as fh:
        fh.write(CLUSTER_ZONE)


command = [
    'gcloud', 'container', '--project',
    PROJECT_ID, 'clusters', 'create', CLUSTER_NAME,
    '--zone', CLUSTER_ZONE,
    '--machine-type', 'e2-medium',
    '--image-type', 'COS',
    '--num-nodes', '1',
    '--enable-stackdriver-kubernetes',
    '--no-enable-ip-alias'
]
res = execute_command(command)
if res != 0:
    print("There was a problem... See message above")
    if not user_prompt_for_confirm(f'You still want to continue?'):
        print("OK, bye bye")
        exit(res)


command = [
    'gcloud', 'container', 'clusters', 'get-credentials', CLUSTER_NAME,
    '--zone', CLUSTER_ZONE, '--project', PROJECT_ID]

res = execute_command(command)
if res != 0:
    print("There was a problem... See message above")
    if not user_prompt_for_confirm(f'You still want to continue?'):
        print("OK, bye bye")
        exit(res)

# create namespace
command = [
    'kubectl', 'create', 'namespace', 'precision-dashboard'
]
execute_command(command)


print('################################################################################')
print('# Create Storage Bucket')
print('################################################################################')
SA_360_OUTPUT_BUCKET = input('Give a name for the bucket: ')
command = ['gsutil', 'mb', f'gs://{SA_360_OUTPUT_BUCKET}']
res = execute_command(command)

while res != 0:
    print("There was a problem... See message above")
    if not user_prompt_for_confirm(f'You still want to continue?'):
        print("OK, bye bye")
        exit(res)
    SA_360_OUTPUT_BUCKET = input('Give a name for the bucket: ')
    command = ['gsutil', 'mb', f'gs://{SA_360_OUTPUT_BUCKET}']
    res = execute_command(command)