import os
import base64
import subprocess

from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent


def execute_command(command: str):
    process = subprocess.Popen(
        command,
        stdout=subprocess.PIPE,
        universal_newlines=True,
        shell=True
    )

    while True:
        output = process.stdout.readline()
        print(output.strip())
        # Do something else
        return_code = process.poll()
        if return_code is not None:
            # Process has finished, read rest of the output
            for output in process.stdout.readlines():
                print(output.strip())
            break


def create_dict_from_env_vars(input_file):
    res = {}
    with open(input_file) as fh:
        content = fh.readlines()

        for line in content:
            try:
                splitted_line = line.split('=')

                key = splitted_line[0]
                value = splitted_line[1].strip('\n')

                value_bytestring = value.encode('ascii')
                value_bytestring_base64 = base64.b64encode(value_bytestring)
                value = value_bytestring_base64.decode('ascii')
                res.update({key: value})
            except IndexError:
                pass

    return res


if __name__ == '__main__':
    #######################################
    # You can use this script to
    # create k8s secrets from files
    #######################################
    import yaml

    execute_command(
        f'cd {os.path.join(BASE_DIR, "templates")} && jq -c . service-account.json > service-account-linted.json')
    with open(os.path.join(BASE_DIR, "templates", "service-account-linted.json"), 'r') as fh:
        res = "service_account.json="
        res = res + fh.read()

    with open(os.path.join(BASE_DIR, "templates", "service-account"), 'w') as fh:
        fh.write(res)

    secrets_yaml_files_to_create = [
        {
            'secret_name': 'env-vars',
            'namespace': 'precision-dashboard',
            'type': 'Opaque',
            'input_file': os.path.join(BASE_DIR, 'templates', 'env-vars')
        },
        {
            'secret_name': 'database-secret',
            'namespace': 'precision-dashboard',
            'type': 'Opaque',
            'input_file': os.path.join(BASE_DIR, 'templates', 'database-secrets')
        },
        {
            'secret_name': 'google-credentials',
            'namespace': 'precision-dashboard',
            'type': 'Opaque',
            'input_file': os.path.join(BASE_DIR, 'templates', 'service-account')
        }
    ]
    for secret in secrets_yaml_files_to_create:
        secret_name = secret.get('secret_name')
        namespace = secret.get('namespace')
        type = secret.get('type')
        input_file = secret.get('input_file')

        res = create_dict_from_env_vars(input_file)

        secrets_yaml = {
            'apiVersion': 'v1',
            'kind': 'Secret',
            'data': res,
            'type': type,
            'metadata': {
                'name': secret_name,
                'namespace': namespace
            }
        }
        yaml_dump = yaml.dump(secrets_yaml)
        with open(os.path.join(BASE_DIR, 'kubernetes', 'secrets', f'secrets-{secret_name}.yaml'), 'w') as fh:
            fh.write(yaml_dump)
