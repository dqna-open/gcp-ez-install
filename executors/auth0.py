import os
import json
import requests

from pathlib import Path
BASE_DIR = Path(__file__).resolve().parent.parent


AUTH0_MANAGEMENT_ID = ''
TOKEN = ''

def delete_applications():
    headers = {
        "Authorization": f"Bearer {TOKEN}",
        "Content-Type": "application/json"
    }
    url = f'{AUTH0_MANAGEMENT_ID}clients'
    res = requests.get(url, headers=headers).json()
    for r in res:
        requests.delete(f'{AUTH0_MANAGEMENT_ID}clients/{r.get("client_id")}', headers=headers)


def create_application(json_file):
    url = f'{AUTH0_MANAGEMENT_ID}clients'
    headers = {
        "Authorization": f"Bearer {TOKEN}",
        "Content-Type": "application/json"
    }
    with open(json_file) as fh:
        request_body = json.loads(fh.read())
        res = requests.post(url, json=request_body, headers=headers)

        ff = res.json()
        return ff


def create_api(json_file):
    url = f'{AUTH0_MANAGEMENT_ID}resource-servers'
    headers = {
        "Authorization": f"Bearer {TOKEN}",
        "Content-Type": "application/json"
    }
    with open(json_file) as fh:
        request_body = json.loads(fh.read())
        res = requests.post(url, json=request_body, headers=headers)

        ff = res.json()

        return ff


def add_roles():
    url = f"{AUTH0_MANAGEMENT_ID}roles"
    headers = {
        "Authorization": f"Bearer {TOKEN}",
        "Content-Type": "application/json"
    }

    roles = [
        {
            "name": "GPD user",
            "description": "GPD user"
        },
        {
            "name": "Super Admin",
            "description": "Super Admin"
        }
    ]
    for role in roles:
        res = requests.post(url, json=role, headers=headers)
        print(res.json())


if __name__ == '__main__':
    AUTH0_MANAGEMENT_ID = input('Please provide the Auth0 Management API ID: ')
    token_file = input('Please enter the location of the file where the token is stored: ')

    with open(os.path.join(BASE_DIR, token_file), 'r') as fh:
        TOKEN = fh.read()
        TOKEN = TOKEN.strip()

    # delete_applications()
    create_application(os.path.join(BASE_DIR, 'executors', 'configs', 'ga-360-ms-application.json'))
    create_application(os.path.join(BASE_DIR, 'executors', 'configs', 'gpd-be-application.json'))
    create_application(os.path.join(BASE_DIR, 'executors', 'configs', 'gpd-fe-application.json'))
    create_api(os.path.join(BASE_DIR, 'executors', 'configs', 'ga-360-ms-api.json'))
    create_api(os.path.join(BASE_DIR, 'executors', 'configs', 'gpd-api.json'))
    add_roles()
