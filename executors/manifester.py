import os
import base64

from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent


def create_dict_from_env_vars(input_file):
    res = {}
    with open(input_file) as fh:
        content = fh.readlines()

        for line in content:
            splitted_line = line.split('=')

            key = splitted_line[0]
            value = splitted_line[1].strip('\n')

            value_bytestring = value.encode('ascii')
            value_bytestring_base64 = base64.b64encode(value_bytestring)
            value = value_bytestring_base64.decode('ascii')
            res.update({key: value})

    return res


if __name__ == '__main__':
    import yaml

    with open(os.path.join(BASE_DIR, 'project_id'), 'r') as fh:
        project_id = fh.read()
        if len(project_id) == 0:
            PROJECT_ID = input("Please provide the Project ID: ")
            TAG_NAME = PROJECT_ID.replace(':', '/')
        else:
            PROJECT_ID = project_id
            TAG_NAME = PROJECT_ID.replace(':', '/')
            print(f"Creating Kubernetes manifests for project: {PROJECT_ID}")

    with open(os.path.join(BASE_DIR, 'image_repo_url_prefix'), 'r') as fh:
        containter_reg_hostname = fh.read()
        if len(containter_reg_hostname) == 0:
            CONTAINER_REGISTRY_HOSTNAME = input("Please provide the Container Registry Hostname [ eg. eu.gcr.io ]: ")
        else:
            CONTAINER_REGISTRY_HOSTNAME = containter_reg_hostname

    print(f"Creating Kubernetes manifests for project: {PROJECT_ID}")

    manifests_api = [
        '02-api-deployment.yaml',
        '02-api-worker-deployment.yaml',
        '02-api-beat-deployment.yaml',
    ]

    for manifest in manifests_api:
        with open(os.path.join(BASE_DIR, 'kubernetes', 'gpd', manifest), 'r') as fh:
            manifest_dict = yaml.load(fh.read(), yaml.SafeLoader)
            manifest_dict['spec']['template']['spec']['containers'][0][
                'image'] = f'{CONTAINER_REGISTRY_HOSTNAME}/{TAG_NAME}/gpd-api:latest'

        yaml_dump = yaml.dump(manifest_dict)
        with open(os.path.join(BASE_DIR, 'kubernetes', 'gpd', manifest), 'w') as fh:
            fh.write(yaml_dump)

    manifests_web = [
        '03-web-deployment.yaml',
    ]

    for manifest in manifests_web:
        with open(os.path.join(BASE_DIR, 'kubernetes', 'gpd', manifest), 'r') as fh:
            manifest_dict = yaml.load(fh.read(), yaml.SafeLoader)
            manifest_dict['spec']['template']['spec']['containers'][0][
                'image'] = f'{CONTAINER_REGISTRY_HOSTNAME}/{TAG_NAME}/gpd-web:latest'

        yaml_dump = yaml.dump(manifest_dict)
        with open(os.path.join(BASE_DIR, 'kubernetes', 'gpd', manifest), 'w') as fh:
            fh.write(yaml_dump)

    manifests_ga360_ms = [
        '01-api-deployment.yaml',
    ]

    for manifest in manifests_ga360_ms:
        with open(os.path.join(BASE_DIR, 'kubernetes', 'ga360ms', manifest), 'r') as fh:
            manifest_dict = yaml.load(fh.read(), yaml.SafeLoader)
            manifest_dict['spec']['template']['spec']['containers'][0][
                'image'] = f'{CONTAINER_REGISTRY_HOSTNAME}/{TAG_NAME}/ga360-ms-api:latest'

        yaml_dump = yaml.dump(manifest_dict)
        with open(os.path.join(BASE_DIR, 'kubernetes', 'ga360ms', manifest), 'w') as fh:
            fh.write(yaml_dump)

    manifests_ga360_ms = [
        'cronjob.yaml',
    ]

    for manifest in manifests_ga360_ms:
        with open(os.path.join(BASE_DIR, 'kubernetes', 'sa360ms', manifest), 'r') as fh:
            manifest_dict = yaml.load(fh.read(), yaml.SafeLoader)
            manifest_dict['spec']['jobTemplate']['spec']['template']['spec']['containers'][0][
                'image'] = f'{CONTAINER_REGISTRY_HOSTNAME}/{TAG_NAME}/sa360-report-ms'

        yaml_dump = yaml.dump(manifest_dict)
        with open(os.path.join(BASE_DIR, 'kubernetes', 'sa360ms', manifest), 'w') as fh:
            fh.write(yaml_dump)