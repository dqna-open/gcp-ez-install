import os.path
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent
"""
This helper can be used to generate a credential file from a authorization process
"""


def create_credentials_from_client_secrets(secrets, credential_file, oauth_scopes):
    # Set up a Flow object to be used if we need to authenticate.
    flow = client.flow_from_clientsecrets(
        secrets, scope=oauth_scopes)
    # Create a file where we will store the credentials
    storage = Storage(credential_file)
    # Run the authorization flown.
    # This wil open a browser and asks for consent of the user.
    args = tools.argparser.parse_args()
    args.noauth_local_webserver = True
    tools.run_flow(flow, storage, args)


if __name__ == '__main__':
    # Define where we want to store the credential file
    credentials = os.path.join(BASE_DIR, 'templates', 'credentials')
    # Define the location of the client secrets
    secrets = os.path.join(BASE_DIR, 'templates', 'client_secrets.json')

    # Adjust the scope as needed
    scopes = [
        'https://mail.google.com/'
    ]
    create_credentials_from_client_secrets(secrets, credentials, scopes)
