import os
import subprocess
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent


def execute_command(command: list):
    process = subprocess.Popen(command,
                               stdout=subprocess.PIPE,
                               universal_newlines=True)

    while True:
        output = process.stdout.readline()
        print(output.strip())
        # Do something else
        return_code = process.poll()
        if return_code is not None:
            # print('RETURN CODE', return_code)
            # Process has finished, read rest of the output
            for output in process.stdout.readlines():
                print(output.strip())
            break


def user_prompt_for_confirm(question: str) -> bool:
    """ Prompt the yes/no-*question* to the user. """
    from distutils.util import strtobool

    while True:
        user_input = input(question + " [y/n]: ").lower()
        try:
            return bool(strtobool(user_input))
        except ValueError:
            print("Please use y/n or yes/no.\n")


CLUSTER_NAME = 'gpd-cluster'
CLUSTER_ZONE = 'europe-west4-a'

with open(os.path.join(BASE_DIR, 'project_id'), 'r') as fh:
    project_id = fh.read()
    if len(project_id) == 0:
        PROJECT_ID = input("Please provide the Project ID: ")
    else:
        PROJECT_ID = project_id
        print(f"Creating secrets for project: {PROJECT_ID}")

# Connect to the cluster
command = [
    'gcloud', 'container', 'clusters', 'get-credentials', CLUSTER_NAME,
    '--zone', CLUSTER_ZONE, '--project', PROJECT_ID
]
execute_command(command)

# Apply the secrets
command = ['kubectl', 'apply', '-f', os.path.join(BASE_DIR, 'kubernetes', 'secrets')]
execute_command(command)
