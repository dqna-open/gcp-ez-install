import subprocess

from pathlib import Path
BASE_DIR = Path(__file__).resolve().parent.parent

def execute_command(command: str):
    process = subprocess.Popen(
        command,
        stdout=subprocess.PIPE,
        universal_newlines=True,
        shell=True
    )

    while True:
        output = process.stdout.readline()
        print(output.strip())
        # Do something else
        return_code = process.poll()
        if return_code is not None:
            # Process has finished, read rest of the output
            for output in process.stdout.readlines():
                print(output.strip())
            break


def user_prompt_for_confirm(question: str) -> bool:
    """ Prompt the yes/no-*question* to the user. """
    from distutils.util import strtobool

    while True:
        user_input = input(question + " [y/n]: ").lower()
        try:
            return bool(strtobool(user_input))
        except ValueError:
            print("Please use y/n or yes/no.\n")


################################################################################
# Download the sources
################################################################################
import os
import tarfile

with open(os.path.join(BASE_DIR, 'project_id'), 'r') as fh:
    project_id = fh.read()
    if len(project_id) == 0:
        PROJECT_ID = input("Please provide the Project ID: ")
        TAG_NAME = PROJECT_ID.replace(':', '/')
    else:
        PROJECT_ID = project_id
        TAG_NAME = PROJECT_ID.replace(':', '/')


with open(os.path.join(BASE_DIR, 'image_repo_url_prefix'), 'r') as fh:
    containter_reg_hostname = fh.read()
    if len(containter_reg_hostname) == 0:
        CONTAINER_REGISTRY_HOSTNAME = input("Please provide the Container Registry Hostname [ eg. eu.gcr.io ]: ")
    else:
        CONTAINER_REGISTRY_HOSTNAME = containter_reg_hostname

print(f"Building Images and push them to project {PROJECT_ID}")

from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent

repos = [

    {
        'docker_image_tag': f'{CONTAINER_REGISTRY_HOSTNAME}/{TAG_NAME}/gpd-web:latest',
        'zip': 'seamless-precision-dashboard-master.tar.gz',
        'unpack_folder': 'seamless-precision-dashboard-master'
    },
    {
        'docker_image_tag': f'{CONTAINER_REGISTRY_HOSTNAME}/{TAG_NAME}/gpd-api:latest',
        'zip': 'seamless-precision-dashboard-master.tar.gz',
        'unpack_folder': 'seamless-precision-dashboard-master'
    },
    {
        'docker_image_tag': f'{CONTAINER_REGISTRY_HOSTNAME}/{TAG_NAME}/ga360-ms-api:latest',
        'zip': 'ga360-api-microservice-master.tar.gz',
        'unpack_folder': 'ga360-api-microservice-master'
    },
    {
        'docker_image_tag': f'{CONTAINER_REGISTRY_HOSTNAME}/{TAG_NAME}/sa360-report-ms:latest',
        'zip': 'sa-360-report-microservice-master.tar.gz',
        'unpack_folder': 'sa-360-report-microservice-master'
    }

]

################################################################################
# Build and push the images
################################################################################
for repo in repos:
    storepath = os.path.join(BASE_DIR, 'data')
    # Dowmload to local disk
    execute_command(f'gsutil cp gs://gpd-source-files/{repo.get("zip")} {storepath}')

    # Extract files from tar-file
    tf = tarfile.open(os.path.join(storepath, repo.get("zip")))
    tf.extractall(storepath)

    if 'gpd-web' in repo.get('docker_image_tag'):
        execute_command(
            f'cp {os.path.join(BASE_DIR, "templates", "auth_config.json")} {os.path.join(storepath, repo.get("unpack_folder"))}/web'
        )
        execute_command(
            f'cd {os.path.join(storepath, repo.get("unpack_folder"))}/web && npm install'
        )
        execute_command(
            f'cd {os.path.join(storepath, repo.get("unpack_folder"))}/web && npm run build'
        )
        execute_command(
            f'gcloud builds submit  --tag {repo.get("docker_image_tag")} {os.path.join(storepath, repo.get("unpack_folder"))}/web --project={PROJECT_ID}'
        )

    elif 'gpd-api' in repo.get('docker_image_tag'):
        execute_command(
            f'gcloud builds submit  --tag {repo.get("docker_image_tag")} {os.path.join(storepath, repo.get("unpack_folder"))}/app --project={PROJECT_ID}'
        )

    else:
        execute_command(
            f'gcloud builds submit  --tag {repo.get("docker_image_tag")} {os.path.join(storepath, repo.get("unpack_folder"))} --project={PROJECT_ID}'
        )
