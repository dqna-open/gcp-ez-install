import os
import base64
import subprocess

from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent


def execute_command(command: str):
    process = subprocess.Popen(
        command,
        stdout=subprocess.PIPE,
        universal_newlines=True,
        shell=True
    )

    while True:
        output = process.stdout.readline()
        print(output.strip())
        # Do something else
        return_code = process.poll()
        if return_code is not None:
            # Process has finished, read rest of the output
            for output in process.stdout.readlines():
                print(output.strip())
            break


def create_dict_from_env_vars(input_file):
    res = {}
    with open(input_file) as fh:
        content = fh.readlines()

        for line in content:
            try:
                key = "credentials.dat"
                value = line.strip('\n')

                value_bytestring = value.encode('ascii')
                value_bytestring_base64 = base64.b64encode(value_bytestring)
                value = value_bytestring_base64.decode('ascii')
                res.update({key: value})
            except IndexError:
                pass

    return res


if __name__ == '__main__':
    #######################################
    # You can use this script to
    # create k8s secrets from files
    #######################################
    import yaml

    secrets_yaml_files_to_create = [
        {
            'secret_name': 'credentials',
            'namespace': 'precision-dashboard',
            'type': 'Opaque',
            'input_file': os.path.join(BASE_DIR, 'templates', 'credentials')
        }
    ]
    for secret in secrets_yaml_files_to_create:
        secret_name = secret.get('secret_name')
        namespace = secret.get('namespace')
        type = secret.get('type')
        input_file = secret.get('input_file')

        res = create_dict_from_env_vars(input_file)

        secrets_yaml = {
            'apiVersion': 'v1',
            'kind': 'Secret',
            'data': res,
            'type': type,
            'metadata': {
                'name': secret_name,
                'namespace': namespace
            }
        }
        yaml_dump = yaml.dump(secrets_yaml)
        with open(os.path.join(BASE_DIR, 'kubernetes', 'secrets', f'secrets-{secret_name}.yaml'), 'w') as fh:
            fh.write(yaml_dump)
